import java.util.List;

public class MassiveObject
{
  // Universal Gravitational Constant
  public final double G = 6.67 * Math.pow(10, -11);
  
  // shared variables
  private  float radius, mass;
  private Point3D position, velocity;
  
  // Gravitational forces from critical masses
  private double[] Fgravs = new double[criticals.length];
  
  // keep track of tracing:
  private boolean isTracing = false;
  
  // construct the massive object
  MassiveObject(float radius, float mass, Point3D position, Point3D velocity)
  {
    this.radius = radius;
    this.mass = mass;
    this.velocity = velocity;
    this.position = position;    

  }
  
  // calculate the overall gravitational acceleration vector acting on the object from all the critical masses
  private Point3D calcGravitationalAcceleration()
  {      
    Point3D gravAcc = new Point3D(0,0,0);

    // calculate gravity of hand, otherwise turn on default gravity
    if (!GRAV_ON)
    {
      this.calculateBigFs();
      // get acceleration due to each critical mass
      for (int i = 0; i < criticals.length; i++)
      {
        double dist = this.position.getDistance(criticals[i].position);
        // Force in x direction = F*(x1 - x2)/d
        double Fx = Fgravs[i] * (criticals[i].position.x - this.position.x) / dist;
        double Fy = Fgravs[i] * (criticals[i].position.y - this.position.y) / dist;
        double Fz = Fgravs[i] * (criticals[i].position.z - this.position.z) / dist;
        
        // update the gravitational acceleration acting on the object. a = F/m
        gravAcc.x += Fx/this.mass;
        gravAcc.y += Fy/this.mass;
        gravAcc.z += Fz/this.mass;
      }
    }
    else gravAcc = new Point3D(0.0, GRAVITY_ACCEL, 0.0); 
    
    
    return gravAcc;
  }
  
  // update the velocity vector acting on the object
  private void updateVelocity()
  {
    final Point3D acc = this.calcGravitationalAcceleration();
    
    // Vupdated = Vcurrent + deltaT*acceleration
    this.velocity.x += deltaT * acc.x;
    this.velocity.y += deltaT * acc.y;
    this.velocity.z += deltaT * acc.z;
  }
  
  // update the position of the object
  private void updatePosition()
  {
    // update the velocity vector acting on the object
    this.updateVelocity(); 
    
    // Xupdated = Xcurr + deltaT * Xvelocity
    this.position.x += deltaT * this.velocity.x;    
    this.position.y += deltaT * this.velocity.y;
    this.position.z += deltaT * this.velocity.z;
    
    // compute bounce for the ball if needed
    this.ricochet();    
  }
  
  // calculate the Gravitational Forces in Newtons from the critical masses
  private void calculateBigFs()
  {
    Fgravs = new double[criticals.length]; 
    for (int i = 0; i < criticals.length; i++)
    {
      // radius between this mass and critical mass
      double r = this.position.getDistance(criticals[i].position);
      // F = GMm/r^2
      Fgravs[i] = (this.G * criticals[i].mass * this.mass) / Math.pow(r, 2);      
    }
  }
  
  
  public void drawObject()
  {
    // update the position of the object based on all forces
    this.updatePosition();
    
    // draw line down to ground
    this.checkTrace();
    
    
    // move to the position
    pushMatrix();
    
    noStroke();
    //fill(randInt(255, 255), randInt(255, 255), randInt(255, 255), 255);
    fill((sin((frameCount)/100.0)+1)*200,(sin((frameCount)/200.0)+1)*200,(sin((frameCount)/50.0)+1)*200);

    
    sphereDetail(5);
    translate(position.x, position.y, position.z);
    rotateZ(radians((frameCount)%360)/2);
    rotateX(radians((frameCount)%360)/2);
    rotateY(radians((frameCount)%360)/2);
    // draw the sphere
    sphere(radius);
    
    popMatrix(); 
  }
  
  // compute bounce on the balls if need be
  private void ricochet()
  {   
    if (key == 'a') return;
    float ric = -.5;
    // keep all the positions within my bounds
    if (position.x < lowXB) {
      position.x = lowXB;
      velocity.x *= ric ; 
    }
    if (position.x > hiXB)  {
      position.x =  hiXB;
      velocity.x *= ric; 
    }
    if (position.y > lowYB) {
      position.y = lowYB;
      velocity.y *= -.33; 
    }
    if (position.y < hiYB)  {
      position.y =  hiYB;
      velocity.y *= ric; 
    }
    if (position.z < lowZB) {
      position.z = lowZB;
      velocity.z *= ric; 
    }
    if (position.z > hiZB)  {
      position.z =  hiZB;
      velocity.z *= ric; 
    }
  }
  
  // add points to the trace
  private void checkTrace()
  {
   if (key == 'd' && !isTracing)
   {
     isTracing = true;
   }
   else if (isTracing)
   {
     pushMatrix();
     //translate(position.x, 0, position.z);
     stroke(0);
     line(position.x,position.y,position.z, position.x, lowYB, position.z);   
     popMatrix();
   }

   // stop tracing
   if (key == 'y')
   {
    isTracing = false; 
   }
  }
  
}
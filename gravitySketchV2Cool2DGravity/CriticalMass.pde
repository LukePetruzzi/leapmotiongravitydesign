

class CriticalMass
{
 float mass;
 Point3D position;
 boolean showMass = false;
 
 CriticalMass(float mass, Point3D position, boolean showMass)
 {
  this.mass = mass; this.position = position; //this.showMass = showMass;
 }
 
 public void drawCriticals()
 {
  // move to the position
  pushMatrix();
  
  stroke(0);
  if (showMass) fill(255, 255, 0, 255);
  else {noFill(); noStroke();}
  
  sphereDetail(10);
  translate(position.x, position.y, position.z);
  rotateY(radians((frameCount)%360)/2);
  // draw the sphere
  sphere(30);
  
  popMatrix(); 
 }
}
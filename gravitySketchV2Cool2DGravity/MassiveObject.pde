

public class MassiveObject
{
  // Universal Gravitational Constant
  public final double G = 6.67 * Math.pow(10, -11);
  
  // shared variables
  final float radius, mass;
  Point3D position, velocity;
  
  // Gravitational forces from critical masses
  private double[] Fgravs = new double[criticals.length];
  
  
  // construct the massive object
  MassiveObject(float radius, float mass, Point3D position, Point3D velocity)
  {
    this.radius = radius;
    this.mass = mass;
    this.velocity = velocity;
    this.position = position;    
  }
  
  // calculate the overall gravitational acceleration vector acting on the object from all the critical masses
  private Point3D calcGravitationalAcceleration()
  {
    this.calculateBigFs();

    Point3D gravAcc = new Point3D(0,0,0);
    
    // get acceleration due to each critical mass
    for (int i = 0; i < criticals.length; i++)
    {
      double dist = this.position.getDistance(criticals[i].position);
      // Force in x direction = F*(x1 - x2)/d
      double Fx = Fgravs[i] * (criticals[i].position.x - this.position.x) / dist;
      double Fy = Fgravs[i] * (criticals[i].position.y - this.position.y) / dist;
      double Fz = Fgravs[i] * (criticals[i].position.z - this.position.z) / dist;
      
      // update the gravitational acceleration acting on the object. a = F/m
      gravAcc.x += Fx/this.mass;
      gravAcc.y += Fy/this.mass;
      gravAcc.z += Fz/this.mass;
    }
    
    return gravAcc;
  }
  
  // update the velocity vector acting on the object
  private void updateVelocity()
  {
    final Point3D acc = this.calcGravitationalAcceleration();
    
    // Vupdated = Vcurrent + deltaT*acceleration
    this.velocity.x += deltaT * acc.x;
    this.velocity.y += deltaT * acc.y;
    this.velocity.z += deltaT * acc.z;
  }
  
  // update the position of the object
  private void updatePosition()
  {
    // update the velocity vector acting on the object
    this.updateVelocity(); 
    
    // Xupdated = Xcurr + deltaT * Xvelocity
    this.position.x += deltaT * this.velocity.x;    
    this.position.y += deltaT * this.velocity.y;
    this.position.z += deltaT * this.velocity.z;
    
    // keep all the positions within my bounds
    if (position.x < lowXB) position.x = lowXB;
    if (position.x > hiXB)  position.x =  hiXB;
    if (position.y > lowYB) position.y = lowYB;
    if (position.y < hiYB)  position.y =  hiYB;
    if (position.z < lowZB) position.z = lowZB;
    if (position.z > hiZB)  position.z =  hiZB;

    
  }
  
  // calculate the Gravitational Forces in Newtons from the critical masses
  private void calculateBigFs()
  {
    Fgravs = new double[criticals.length]; 
    for (int i = 0; i < criticals.length; i++)
    {
      // radius between this mass and critical mass
      double r = this.position.getDistance(criticals[i].position);
      // F = GMm/r^2
      Fgravs[i] = (this.G * criticals[i].mass * this.mass) / Math.pow(r, 2);      
    }
  }
  
  
  public void drawObject()
  {
    // update the position of the object based on all forces
    this.updatePosition();
    
    // move to the position
    pushMatrix();
    
    noStroke();
    //fill(randInt(255, 255), randInt(255, 255), randInt(255, 255), 255);
    fill((sin((frameCount)/100.0)+1)*200,(sin((frameCount)/200.0)+1)*200,(sin((frameCount)/50.0)+1)*200);

    
    sphereDetail(5);
    translate(position.x, position.y, position.z);
    rotateZ(radians((frameCount)%360)/2);
    rotateX(radians((frameCount)%360)/2);
    rotateY(radians((frameCount)%360)/2);
    // draw the sphere
    sphere(radius);
    
    popMatrix(); 
  }
}
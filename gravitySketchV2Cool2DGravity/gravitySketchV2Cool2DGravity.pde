// for camera
import peasy.*;
// for 3D
import shapes3d.*;
import shapes3d.animation.*;
// for leap
import de.voidplus.leapmotion.*;

import java.util.Random;

public final int OBJECTS_COUNT = 4000;
public final float FINGER_MASS = 1.0E17;
public final float GRAVITY_MASS = 1.0E19;
// bounds
public int lowXB;
public int hiXB;
public int lowYB;
public int hiYB;
public int lowZB;
public int hiZB;
// should down gravity be on??
public boolean GRAV_ON = true;


// can access critical masses from everywhere
public CriticalMass[] criticals = new CriticalMass[0];
// the time between frames (not built in for some god awful reason)
public double deltaT;

private MassiveObject[] massiveObjects = new MassiveObject[OBJECTS_COUNT];

// for calculating deltaT
private double prevTime;
private double currTime;

private PeasyCam cam;
private LeapMotion leap;

// keep track of the amount of fingers per frame
private int fingersPresent = 0;

void setup() 
{
  background(0);
  size(1440, 800, P3D);
  
  // set bounds
  lowXB = -width/2;
  hiXB = width/2;
  lowYB = height*2;
  hiYB = height*-2;
  lowZB = lowXB;
  hiZB = hiXB;
  
  // create camera and set it up
  cam = new PeasyCam(this, hiXB + lowXB, height/2, 0, 4000);
  
  // create new leap motion
  leap = new LeapMotion(this);

  // create a bunch of random objects
  for (int i = 0; i < OBJECTS_COUNT; i++)
  {
   massiveObjects[i] = new MassiveObject(5, 1.0E5, new Point3D(randInt(lowXB, hiXB), randInt(lowYB-50, lowYB), randInt(lowZB, hiZB)), 
                                         new Point3D(0,0,0));
  }
}


void draw()
{
  // background
  background(255);
  // get change in time b/w frames
  this.calculateDeltaT(); 
  // create lights
  directionalLight(255, 255, 255, -1, 1, -1);
  this.doLeapStuff(); // do the essential leap motion things
  
  for (int i = 0; i < criticals.length; i++)
  {
   criticals[i].drawCriticals(); 
  }
  
  // draw all the objects
  for (int i = 0; i < massiveObjects.length; i++)
  {
   massiveObjects[i].drawObject(); 
   
   //float cDist = Float.POSITIVE_INFINITY;
   //Point3D closest = new Point3D(0,0,0);
   //for (int j = 0; j < massiveObjects.length; j++)
   //{
   //  if (j == i) continue;
   //  float next = (float)massiveObjects[i].position.getDistance(massiveObjects[j].position);
   //  if(next < cDist)
   //  {
   //    cDist = next;
   //    closest = massiveObjects[j].position;
   //  } 
   //}
      
   //// draw lines
   //Point3D m1 = massiveObjects[i].position;
   //Point3D m2 = closest;
   //if (m1.getDistance(m2) < 25)
   //{
   //  stroke(0);
   //  line(m1.x, m1.y, m1.z, m2.x, m2.y, m2.z);
   //}
  }
}

// calculate deltaT
private void calculateDeltaT()
{
 this.currTime = millis();
 deltaT = (currTime - prevTime) / 1000.0;
 this.prevTime = currTime;
}

private void doLeapStuff()
{
  int fps = leap.getFrameRate();
  
  // When the hand is NOT present
  if (leap.getHands().size() == 0) 
  {
    fingersPresent = 0;
    // turn gravity on in absence of hand
    GRAV_ON = true;
    criticals = new CriticalMass[6];
    criticals[0] = new CriticalMass(GRAVITY_MASS, new Point3D(lowXB*0.75, lowYB*2, lowZB*.75), true);
    criticals[1] = new CriticalMass(GRAVITY_MASS, new Point3D(lowXB*0.75, lowYB*2, hiZB*.75), true);
    criticals[2] = new CriticalMass(GRAVITY_MASS, new Point3D(hiXB*0.75, lowYB*2, lowZB*.75), true);
    criticals[3] = new CriticalMass(GRAVITY_MASS, new Point3D(hiXB*0.75, lowYB*2, hiZB*.75), true);
    //criticals[4] = new CriticalMass(GRAVITY_MASS, new Point3D(0, lowYB*2, 0), true);
    criticals[4] = new CriticalMass(GRAVITY_MASS, new Point3D(lowXB*0.25, lowYB*2, lowZB*.25), true);
   // criticals[6] = new CriticalMass(GRAVITY_MASS, new Point3D(lowXB*0.25, lowYB*2, hiZB*.25), true);
    criticals[5] = new CriticalMass(GRAVITY_MASS, new Point3D(hiXB*0.25, lowYB*2, lowZB*.25), true);
   // criticals[8] = new CriticalMass(GRAVITY_MASS, new Point3D(hiXB*0.25, lowYB*2, hiZB*.25), true);


  }
  else { GRAV_ON = false;} // turn gravity on otherwise
  
  for (Hand hand : leap.getHands ()) 
  {
    int fingersPrev = fingersPresent;
    fingersPresent = hand.getFingers().size();
    
    // get outstretched
    if (keyPressed && fingersPresent != 0)
    {
      fingersPresent = 1;
    }
    
    // create new criticals if there are now different amount of fingers
    if (fingersPresent != fingersPrev)
    {
       criticals = new CriticalMass[fingersPresent];
       println("CREATED NEW CRITICALS ARRAY OF SIZE: "+hand.getFingers().size());
    }

    for (int i = 0; i < fingersPresent; i++) 
    {       
       Bone boneDistal = hand.getFinger(i).getDistalBone();
       
       PVector tipJoint = boneDistal.getNextJoint();
       
       criticals[i] = new CriticalMass(FINGER_MASS, new Point3D(tipJoint.x, tipJoint.y, tipJoint.z), true);
    }
  }
}

// create a random int in range
public int randInt(int min, int max) {
    Random rand = new Random();
    int randomNum = rand.nextInt((max - min) + 1) + min;
    return randomNum;
}
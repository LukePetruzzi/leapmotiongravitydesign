// 3D point class
public class Point3D
{
 float x, y, z;
 
 public Point3D(float x, float y, float z)
 {
  this.x = x; this.y = y; this.z = z;
 }
 
 // get distance between this point and another point
 public double getDistance(Point3D other)
 {
   double argument = Math.abs(Math.pow(this.x - other.x, 2) +
                              Math.pow(this.y - other.y, 2) +
                              Math.pow(this.z - other.z, 2));
   return (double) Math.sqrt(argument);
 }
 
 public String toString()
 {
  return new String("X: "+this.x+", Y: "+this.y+", Z: "+this.z);
 }

}
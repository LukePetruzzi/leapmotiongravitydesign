// for camera
import peasy.*;
// for 3D
import shapes3d.*;
import shapes3d.animation.*;
// for leap
import de.voidplus.leapmotion.*;
import processing.pdf.*;
import java.util.Random;

public final int OBJECTS_COUNT = 3000;
public final float FINGER_MASS = 1.0E17;
public final float GRAVITY_ACCEL = 600.0;
// bounds
public int lowXB;
public int hiXB;
public int lowYB;
public int hiYB;
public int lowZB;
public int hiZB;
// should down gravity be on??
public boolean GRAV_ON = true;


// can access critical masses from everywhere
public CriticalMass[] criticals = new CriticalMass[0];
// the time between frames (not built in for some god awful reason)
public double deltaT;

private MassiveObject[] massiveObjects = new MassiveObject[OBJECTS_COUNT];

// for calculating deltaT
private double prevTime;
private double currTime;

private PeasyCam cam;
private LeapMotion leap;

// keep track of the amount of fingers per frame
private int fingersPresent = 0;

void setup() 
{
  background(0);
  //size(1440, 800, P3D);
  fullScreen(P3D);
  
  // for PDFFING
  //beginRaw(PDF, "raw.pdf");

  
  // set bounds
  lowXB = -width;
  hiXB = width;
  lowYB = height*2;
  hiYB = height*-2;
  lowZB = lowXB;
  hiZB = hiXB;
  
  // create camera and set it up
  cam = new PeasyCam(this, hiXB + lowXB, height/2, 0, 4000);
  
  // create new leap motion
  leap = new LeapMotion(this);
  // set leap's world position
  leap.moveWorld((int)(-width*.50), height/2, 0);

  // create a bunch of random objects
  for (int i = 0; i < OBJECTS_COUNT; i++)
  {
     massiveObjects[i] = new MassiveObject(10, 1.0E5, new Point3D(randInt(lowXB, hiXB), randInt(lowYB-50, lowYB), randInt(lowZB, hiZB)), 
                                         new Point3D(0,0,0));
  }
}


void draw()
{
  // background
  background(255);
  // get change in time b/w frames
  this.calculateDeltaT(); 
  // create lights
  directionalLight(255, 255, 255, -1, 1, -1);
  // create a nice little platform
  this.createPlatform();

  
  for (int i = 0; i < criticals.length; i++)
  {
   criticals[i].drawCriticals(); 
  }
  
  // draw all the objects
  for (int i = 0; i < massiveObjects.length; i++)
  {
   massiveObjects[i].drawObject(); 
   //massiveObjects[i].drawTrace();
  }

  this.doLeapStuff(); // do the essential leap motion things

}

// calculate deltaT
private void calculateDeltaT()
{
 this.currTime = millis();
 deltaT = (currTime - prevTime) / 1000.0;
 this.prevTime = currTime;
}

private void doLeapStuff()
{
  int fps = leap.getFrameRate();
  
  // When the hand is NOT present
  if (leap.getHands().size() == 0) 
  {
    fingersPresent = 0;
    // turn gravity on in absence of hand
    GRAV_ON = true;
    criticals = new CriticalMass[0];
  }
  else { GRAV_ON = false;} // turn gravity on otherwise
  
  for (Hand hand : leap.getHands ()) 
  {    
    int fingersPrev = fingersPresent;
    fingersPresent = hand.getFingers().size();
    
    // get outstretched
    if (fingersPresent != 0 && key == 'z')
    {
      fingersPresent = 1;
    }
    
    // create new criticals if there are now different amount of fingers
    if (fingersPresent != fingersPrev)
    {
       criticals = new CriticalMass[fingersPresent];
       println("CREATED NEW CRITICALS ARRAY OF SIZE: "+hand.getFingers().size());
    }

    // do the index if that's what it is
    if (fingersPresent == 1) {
       Bone boneDistal = hand.getFinger(1).getDistalBone();
       PVector tipJoint = boneDistal.getNextJoint();
       criticals[0] = new CriticalMass(FINGER_MASS, new Point3D(tipJoint.x, tipJoint.y, tipJoint.z), true);
       
       hand.drawFingers();
       return;
    }


    for (int i = 0; i < fingersPresent; i++) 
    {       
       Bone boneDistal = hand.getFinger(i).getDistalBone();
       
       PVector tipJoint = boneDistal.getNextJoint();
       
       criticals[i] = new CriticalMass(FINGER_MASS, new Point3D(tipJoint.x, tipJoint.y , tipJoint.z), true);
    }
    hand.drawFingers();
    
  }
}

private void createPlatform()
{
  // dont draw if a is pressed
  if (key == 'a')
  {
    return;
  }
  else
  {
    // draw a nice little platform
    pushMatrix();
    noStroke();
    fill((cos((frameCount)/100.0)+1)*200,(cos((frameCount)/200.0)+1)*200,(cos((frameCount)/50.0)+1)*200, 100);
    translate(-width - massiveObjects[0].radius * 2, (height*2) + massiveObjects[0].radius*2, -width - massiveObjects[0].radius * 2);
    rotateX(PI/2);
    rect(0, 0, width*2 + massiveObjects[0].radius * 2, width*2 + massiveObjects[0].radius * 2);
    popMatrix();
  }
}
// create a random int in range
public int randInt(int min, int max) 
{
    Random rand = new Random();
    int randomNum = rand.nextInt((max - min) + 1) + min;
    return randomNum;
}

void keyPressed() {
  if (key == ' ') {
    endRaw();
    exit();
  }
}